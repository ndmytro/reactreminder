
class Event extends React.Component {
  render() {
    return (
      <div className="event">
        <h2 className="eventName">
          {this.props.name}
        </h2>
        {this.props.children}
      </div>
    );
  }
}

class EventList extends React.Component {

  render() {
    var eventNodes = this.props.data.map(function(event) {
      var dateObj = new Date(event.date);
      var year = dateObj.getFullYear();
      var month = dateObj.getMonth();
      var day = dateObj.getDate();
      var hours = dateObj.getUTCHours() + 2;
      var minutes = dateObj.getMinutes();
      
      return (
        <Event name={event.name} key={event.id}>
          Date: {year}.{month}.{day}, Hours: {hours}, Minutes: {minutes}
          <p>{event.text}</p>
        </Event>
      );
    });
    return (
      <div className="eventList">
        {eventNodes}
      </div>
    );
  }
}

class EventForm extends React.Component {
    
    constructor(props) {
        super(props);
        this.state = {author: '', text: '', date: ''};
    }
    
    render() {
        return (
          <form className="eventForm" onSubmit={(e) => this.handleSubmit(e)}>
            <input
              type="text"
              placeholder="Event..."
              value={this.state.name}
              onChange={(e) => this.handleNameChange(e)}
            />
            <input
              type="text"
              placeholder="Event description..."
              value={this.state.text}
              onChange={(e) => this.handleTextChange(e)}
            />
            <input
              type="text"
              placeholder="Date, DD MM YYYY, optional HH MM"
              value={this.state.date}
              onChange={(e) => this.handleDateChange(e)}
            />
            <input type="submit" value="Add event!" />
          </form>
        );
    }
    
    handleNameChange(e) {
        this.setState({name: e.target.value});
    }
    
    handleTextChange(e) {
        this.setState({text: e.target.value});
    }
    
    handleDateChange(e) {
        this.setState({date: e.target.value});
    }
    
    dateSplitter(date) {
        var dateParts = date.split(/\.|\s|:/);
        	if (dateParts.length == 5) {
        		date = new Date(Date.UTC(dateParts[2], dateParts[1] -1, dateParts[0], dateParts[3]-2, dateParts[4]));
        	} else if (dateParts.length == 3) {
                date = new Date(Date.UTC(dateParts[2], dateParts[1] -1, dateParts[0]));
        	};
        if (typeof date == "object") {
            return date;
        } else {
        return false;
        }
    }
    
    handleSubmit(e) {
        e.preventDefault(); 
        
        var name = this.state.name.trim();
        var text = this.state.text.trim();
        var date = this.dateSplitter(this.state.date.trim());
        var fired = false;
        if (!text || !name || !date) {
          return;
        }
        var gotFromLS = eval(localStorage.getItem('eventsObject'));
        console.log(typeof gotFromLS);
        gotFromLS.push({name: name, text: text, date: date, fired: fired});
        localStorage.setItem('eventsObject', JSON.stringify(gotFromLS));
        this.setState({name: '', text: '', date: ''});
    }
}

class EventBox extends React.Component {
   constructor(props) {
    super(props);
    this.state = {data: []};
   }
  
  componentDidMount() {
    console.log("did mount!")
    this.loadEventsFromLocalStorage();
    setInterval(() => this.loadEventsFromLocalStorage(), 1000);
  }
  
  loadEventsFromLocalStorage() {
      this.state
      var data = eval(localStorage.getItem('eventsObject'));
      
      data.forEach(function(event) {

          console.log(event.name+":"+typeof event.date+":"+event.date+":date:"+new Date(event.date)+":now date:"+new Date+":fired:"+event.fired);
          if (new Date(event.date) < new Date && !event.fired) {
              alert("You have an event: " + event.name + ". Here is a description: " + event.text);
              event.fired = true;
              localStorage.setItem('eventsObject', JSON.stringify(data));       
          };
      });
      this.setState({data});
  }
  
  render() {
    return (
      <div className="eventBox">
        <h1>Events</h1>
        <EventList data={this.state.data} />
        <EventForm />
      </div>
    );
  }
}



const data = [
  {id: 1, name: "Petya B-Day", text: "Prepare to celebrate", date: new Date("2011.12.12"), fired: false},
  {id: 2, name: "Vanya B-Day", text: "Don't forget about Vanya", date: new Date("2016.12.12"), fired: false}
];

localStorage.setItem('eventsObject', JSON.stringify(data));

ReactDOM.render(
  <EventBox/>,
  document.getElementById('content')
);